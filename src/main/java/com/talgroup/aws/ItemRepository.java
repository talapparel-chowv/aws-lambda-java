package com.talgroup.aws;

import java.util.List;
import java.util.Optional;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;

@Repository
public interface ItemRepository extends EntityRepository<Item, Long> {

	List<Item> findByKey(final String key);

	@Query("select i from Item i where i.value = ?1")
	Optional<Item> findByValue(final String value);
}
